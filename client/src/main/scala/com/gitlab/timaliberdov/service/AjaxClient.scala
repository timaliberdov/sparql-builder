package com.gitlab.timaliberdov.service

import com.gitlab.timaliberdov.model
import com.gitlab.timaliberdov.model._
import io.circe.Decoder
import io.circe.parser.parse
import io.circe.syntax._
import org.scalajs.dom.XMLHttpRequest
import org.scalajs.dom.ext.Ajax

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

object AjaxClient {

  private val baseUrl = "/api"

  def getAllClasses: Future[List[model.Class]] =
    Ajax.get(s"$baseUrl/classes").parseEntity[List[model.Class]]

  def getAllLabelledClasses: Future[List[model.Class]] =
    Ajax.get(s"$baseUrl/classes/labelled").parseEntity[List[model.Class]]

  def loadProperties: Future[List[LabelledPredicate]] =
    Ajax.get(s"$baseUrl/props").map{ r =>
      println(r.responseText)
      r
    }.parseEntity[List[LabelledPredicate]]

  def getQuery(request: QueryRequest): Future[String] =
    Ajax.post(s"$baseUrl/query", data = request.asJson.noSpaces).map(_.responseText)

  def getResults(request: QueryRequest): Future[List[QueryResponse]] =
    Ajax.post(s"$baseUrl/query/results", data = request.asJson.noSpaces).parseEntity[List[QueryResponse]]

  implicit class RequestFutureExt(f: Future[XMLHttpRequest]) {
    def parseEntity[A: Decoder]: Future[A] =
      f.flatMap(req => Future.fromTry(parse(req.responseText).right.flatMap(_.as[A]).toTry))
  }

}
