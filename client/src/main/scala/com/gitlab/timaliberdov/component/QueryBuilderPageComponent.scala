package com.gitlab.timaliberdov.component

import com.gitlab.timaliberdov.model
import com.gitlab.timaliberdov.model._
import com.gitlab.timaliberdov.page.Page
import com.gitlab.timaliberdov.service.AjaxClient
import japgolly.scalajs.react._
import japgolly.scalajs.react.component.Scala.{BackendScope, Unmounted}
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.scalajs.dom
import org.scalajs.dom.ext._
import org.scalajs.dom.html.{Element, Input, Select}
import org.scalajs.dom.raw.HTMLElement

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js

object QueryBuilderPageComponent {

  final case class Props(ctl: RouterCtl[Page])
  final case class State(
    classes: List[model.Class] = Nil,
    query: String = "",
    propsToFindInputs: Int = 1,
    knownPropsInputs: Int = 1,
    paramOptions: List[LabelledPredicate] = Nil,
    results: List[QueryResponse] = Nil)

  final class Backend(bs: BackendScope[Props, State]) {

    private def updateClasses(classes: List[model.Class]): Callback = bs.modState(_.copy(classes = classes))

    private def setVisible(selector: String, visible: Boolean = true): Unit =
      dom.document.querySelector(selector) match {
        case element: HTMLElement =>
          element.style.display = if (visible) "block" else "none"
        case _ =>
      }

    private[QueryBuilderPageComponent] def updateProperties(state: State): Callback = {
      if (state.paramOptions.nonEmpty) {
        Callback.empty
      } else {
        Callback(setVisible(".query-builder-page", visible = false)) >>
          Callback(setVisible(".loader")) >>
          Callback.future {
            AjaxClient.loadProperties.recover { case _ =>
              List.empty[LabelledPredicate]
            }.map { ps =>
              bs.modState(s => s.copy(paramOptions = ps)) >>
                Callback(setVisible(".loader", visible = false)) >>
                Callback(setVisible(".query-builder-page"))
            }
          }
      }
    }

    private def updateQuery(e: ReactEventFromInput): Callback = e.preventDefaultCB >> {
      def collectSelectValuesByName(name: String): List[String] =
        dom.document.getElementsByName(name).toList.collect {
          case e: Select => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      def collectInputValuesByName(name: String): List[String] =
        dom.document.getElementsByName(name).toList.collect {
          case e: Input => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      val propsToFind: List[Prop] =
        collectSelectValuesByName("props-to-find-select").map(Prop(_))
      val knownProps: List[PropWithVal] = {
        val props = collectSelectValuesByName("known-props-select")
        val values = collectInputValuesByName("known-props-values-input")
        props.zip(values).map {
          case (prop, value) => PropWithVal(Prop(prop), value)
        }
      }
      Callback.future {
        AjaxClient.getQuery(QueryRequest(propsToFind, knownProps)).recover {
          case e => println(e.getStackTrace.mkString(System.lineSeparator()))
            "Error, sorry"
        }.map(value => bs.modState(_.copy(query = value)))
      }
    }

    def updateResults(e: ReactEventFromInput): Callback = e.preventDefaultCB >> {
      def collectSelectValuesByName(name: String): List[String] =
        dom.document.getElementsByName(name).toList.collect {
          case e: Select => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      def collectInputValuesByName(name: String): List[String] =
        dom.document.getElementsByName(name).toList.collect {
          case e: Input => Option(e.value)
        }.flatten.map(_.trim).filter(_.nonEmpty)

      val propsToFind: List[Prop] =
        collectSelectValuesByName("props-to-find-select").map(Prop(_))
      val knownProps: List[PropWithVal] = {
        val props = collectSelectValuesByName("known-props-select")
        val values = collectInputValuesByName("known-props-values-input")
        props.zip(values).map {
          case (prop, value) => PropWithVal(Prop(prop), value)
        }
      }
      Callback.future {
        AjaxClient.getResults(QueryRequest(propsToFind, knownProps)).recover {
          case e => println(e.getStackTrace.mkString(System.lineSeparator()))
            List.empty[QueryResponse]
        }.map(results => bs.modState(_.copy(results = results)))
      }
    }

    def render(props: Props, state: State): VdomTagOf[Element] =
      <.section(
        <.div(
          ^.cls := "container query-builder-page",
          <.div(
            <.h2("SPARQL Query Builder"),
            <.h5(
              ^.cls := "mb-3",
              "Инструмент для конструирования и выполнения запросов по заданной онтологии"
            )
          ),
          <.div(
            ^.cls := "row",
            <.div(
              ^.cls := "column",
              <.table(
                <.thead(
                  <.tr(<.th("Параметры, значения которых нужно найти"))
                ),
                <.tbody(
                  (1 to state.propsToFindInputs).map { _ =>
                    <.tr(
                      <.td(
                        <.div(
                          ^.cls := "input-group",
                          <.select(
                            ^.cls := "custom-select",
                            ^.name := "props-to-find-select",
                            <.option(^.hidden := true),
                            state.paramOptions.map(p => <.option(p.pred)).toTagMod // todo: label
                          )
                        )
                      ),
                      <.td(
                        <.button(
                          ^.`type` := "button",
                          ^.cls := "btn btn-default btn-circle ml-1",
                          ^.onClick ==> (e => e.preventDefaultCB >> bs.modState(s => s.copy(propsToFindInputs = s.propsToFindInputs + 1))),
                          <.i(^.cls := "fas fa-plus")
                        )
                      )
                    )
                  }.toTagMod
                )
              )
            ),
            <.div(
              ^.cls := "column",
              <.table(
                <.thead(
                  <.tr(
                    <.th("Известные параметры"),
                    <.th("Значения")
                  )
                ),
                <.tbody(
                  (1 to state.knownPropsInputs).map { _ =>
                    <.tr(
                      <.td(
                        ^.style := js.Dictionary("width" -> "50%"),
                        <.div(
                          ^.cls := "input-group",
                          <.select(
                            ^.cls := "custom-select",
                            ^.name := "known-props-select",
                            <.option(^.hidden := true),
                            state.paramOptions.map(p => <.option(p.pred)).toTagMod // todo: label
                          )
                        )
                      ),
                      <.td(
                        ^.style := js.Dictionary("width" -> "50%"),
                        <.input(^.`type` := "text", ^.cls := "form-control", ^.name := "known-props-values-input")
                      ),
                      <.td(
                        <.button(
                          ^.`type` := "button",
                          ^.cls := "btn btn-default btn-circle ml-1",
                          ^.onClick ==> (e => e.preventDefaultCB >> bs.modState(s => s.copy(knownPropsInputs = s.knownPropsInputs + 1))),
                          <.i(^.cls := "fas fa-plus")
                        )
                      )
                    )
                  }.toTagMod
                )
              )
            )
          ),
          <.div(
            ^.role := "tabpanel",
            <.div(
              ^.role := "tabpanel",
              <.ul(
                ^.cls := "nav nav-tabs",
                ^.role := "tablist",
                <.li(
                  ^.cls := "nav-item",
                  ^.role := "presentation",
                  <.a(
                    ^.cls := "nav-link",
                    ^.href := "#results",
                    ^.aria.controls := "results",
                    ^.role := "tab",
                    VdomAttr("data-toggle") := "tab",
                    "Результаты"
                  )
                ),
                <.li(
                  ^.cls := "nav-item",
                  ^.role := "presentation",
                  <.a(
                    ^.cls := "nav-link",
                    ^.href := "#query",
                    ^.aria.controls := "query",
                    ^.role := "tab",
                    VdomAttr("data-toggle") := "tab",
                    "SPARQL запрос"
                  )
                )
              )
            ),
            <.div(
              ^.cls := "tab-content",
              <.div(
                ^.role := "tabpanel",
                ^.cls := "container tab-pane",
                ^.id := "results",
                <.div(
                  <.button(
                    ^.`type` := "button",
                    ^.cls := "btn btn-default btn-circle btn-right",
                    ^.onClick ==> updateResults,
                    <.i(^.cls := "fas fa-sync-alt")
                  )
                ),
                <.div(
                  <.table(
                    ^.cls := "table",
                    <.thead(
                      <.tr(
                        ^.cls := "thead-dark",
                        <.th(
                          ^.scope := "col",
                          "Субъект"
                        ),
                        <.th(
                          ^.scope := "col",
                          "Предикат"
                        ),
                        <.th(
                          ^.scope := "col",
                          "Объект"
                        )
                      )
                    ),
                    <.tbody(
                      state.results.map { res =>
                        <.tr(
                          <.td(
                            <.div(
                              <.p(res.subj.subj)//,
  //                            res.subj.label.map(<.p(_)).whenDefined
                            )
                          ),
                          <.td(
                            <.div(
                              <.p(res.pred.pred)//,
  //                            res.pred.label.map(<.p(_)).whenDefined
                            )
                          ),
                          <.td(
                            <.div(
                              <.p(res.obj.obj)//,
  //                            res.obj.label.map(<.p(_)).whenDefined
                            )
                          )
                        )
                      }.toTagMod
                    )
                  )
                )
              ),
              <.div(
                ^.role := "tabpanel",
                ^.cls := "container tab-pane",
                ^.id := "query",
                <.div(
                  <.button(
                    ^.`type` := "button",
                    ^.cls := "btn btn-default btn-circle btn-right",
                    ^.onClick ==> updateQuery,
                    <.i(^.cls := "fas fa-sync-alt")
                  )
                ),
                <.div(
                  <.textarea(
                    ^.cls := "form-control monospaced",
                    ^.rows := 20,
                    ^.disabled := true,
                    ^.value := state.query
                  )
                )
              )
            )
          )
        ),
        <.div(^.cls := "loader")
      )
  }

  private val component = ScalaComponent
    .builder[Props]("QueryBuilderPage")
    .initialState(State())
    .renderBackend[Backend]
    .componentDidMount(cdm => cdm.backend.updateProperties(cdm.state))
    .build

  def apply(ctl: RouterCtl[Page]): Unmounted[Props, State, Backend] =
    component(Props(ctl))

}
