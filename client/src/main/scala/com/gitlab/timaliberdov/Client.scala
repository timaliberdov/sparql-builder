package com.gitlab.timaliberdov

import com.gitlab.timaliberdov.page._
import com.gitlab.timaliberdov.component.QueryBuilderPageComponent
import japgolly.scalajs.react.extra.router._
import japgolly.scalajs.react.vdom.VdomElement
import org.scalajs.dom

object Client {

  def main(args: Array[String]): Unit = {
    val container = dom.document.getElementById("root")
    val router = Router(BaseUrl.fromWindowOrigin_/, routerConfig)
    router() renderIntoDOM container
  }

  private val routerConfig = RouterConfigDsl[Page].buildConfig { dsl =>
    import dsl._

    (trimSlashes
      | staticRoute("#builder", QueryBuilderPage) ~> renderR(QueryBuilderPageComponent(_))
      | staticRedirect(root) ~> redirectToPage(QueryBuilderPage)(Redirect.Replace)
      )
      .notFound(redirectToPage(QueryBuilderPage)(Redirect.Replace))
      .verify(QueryBuilderPage)
      .renderWith(layout)
  }

  private def layout(ctl: RouterCtl[Page], resolution: Resolution[Page]): VdomElement =
    resolution.render()

}
