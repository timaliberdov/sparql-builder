package com.gitlab.timaliberdov

package object page {

  sealed trait Page
  case object QueryBuilderPage extends Page

}
