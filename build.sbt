import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}

val commonSettings = Seq(
  version := "0.1",
  isSnapshot := true,
  scalaVersion := "2.12.8",
  scalacOptions := Seq(
    // format off
    "-deprecation",           // Emit warning and location for usages of deprecated APIs.
    "-encoding", "utf-8",     // Specify character encoding used by source files.
    "-explaintypes",          // Explain type errors in more detail.
    "-feature",               // Emit warning and location for usages of features that should be imported explicitly.
    "-language:higherKinds",  // Allow higher-kinded types
    "-Xcheckinit",            // Wrap field accessors to throw an exception on uninitialized access.
    "-Xfatal-warnings",       // Fail the compilation if there are any warnings.
    "-Ypartial-unification"   // Enable partial unification in type constructor inference
    // format: on
  ),
  parallelExecution in Test := false)

lazy val shared = (crossProject(JSPlatform, JVMPlatform) crossType CrossType.Pure in file("shared"))
  .settings(commonSettings)
  .settings(libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-effect" % "1.2.0",
    "io.monix" %%% "monix" % "3.0.0-RC2",
    "com.lihaoyi" %%% "scalatags" % "0.6.8"))
  .settings(libraryDependencies ++=
    Seq("core", "generic", "generic-extras", "optics", "parser")
      .map(module => "io.circe" %%% s"circe-$module" % "0.11.0"))
  .settings(libraryDependencies += "io.circe" %%% "circe-derivation" % "0.11.0-M1")
  .jsConfigure(_ enablePlugins ScalaJSWeb)

lazy val sharedJS  = shared.js
lazy val sharedJVM = shared.jvm

val additionalAssets = taskKey[Seq[File]]("copy additional assets to managed resources")

lazy val client = (project in file("client"))
  .settings(commonSettings)
  .settings(
    skip in packageJSDependencies := false,
    scalaJSUseMainModuleInitializer := true,
    dependencyOverrides += "org.webjars.npm" % "js-tokens" % "3.0.2")
  .settings(additionalAssets := {
    val assetsDir = (resourceDirectory in Compile).value / "assets"
    val assets = assetsDir ** "*.*"
    val mappings = assets pair Path.relativeTo(assetsDir)
    val copies = mappings map { case (file, path) => file -> (resourceManaged in Assets).value / path }
    IO.copy(copies)
    copies map (_._2)
  })
  .settings(libraryDependencies ++= Seq(
    "io.suzaku" %%% "diode" % "1.1.4",
    "io.suzaku" %%% "diode-react" % "1.1.4.131",
    "org.scala-js" %%% "scalajs-dom" % "0.9.6",
    "org.querki" %%% "jquery-facade" % "1.2"))
  .settings(libraryDependencies ++=
    Seq("core", "extra")
      .map(module => "com.github.japgolly.scalajs-react" %%% module % "1.4.1"))
  .settings(jsDependencies ++= Seq(
    "org.webjars.npm" % "react" % "16.8.5"
      / "umd/react.development.js"
      minified "umd/react.production.min.js"
      commonJSName "React",
    "org.webjars.npm" % "react-dom" % "16.8.5"
      / "umd/react-dom.development.js"
      minified "umd/react-dom.production.min.js"
      dependsOn "umd/react.development.js"
      commonJSName "ReactDOM",
    "org.webjars.npm" % "react-dom" % "16.8.5"
      / "umd/react-dom-server.browser.development.js"
      minified "umd/react-dom-server.browser.production.min.js"
      dependsOn "umd/react-dom.development.js"
      commonJSName "ReactDOMServer"))
  .dependsOn(sharedJS)
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)

lazy val server = (project in file("server"))
  .settings(commonSettings)
  .settings(libraryDependencies ++=
    Seq("core", "dsl", "circe", "blaze-server", "blaze-client", "scalatags")
      .map(module => "org.http4s" %% s"http4s-$module" % "0.20.0"))
  .settings(libraryDependencies ++= Seq(
    "com.vmunier" %% "scalajs-scripts" % "1.1.2",
    "com.github.pathikrit" %% "better-files" % "3.8.0",
    "com.github.scopt" %% "scopt" % "4.0.0-RC2",
    "com.github.pureconfig" %% "pureconfig" % "0.12.1",
    "ch.qos.logback" % "logback-classic" % "1.2.3"
      exclude("org.slf4j", "slf4j-api")
      exclude("org.slf4j", "slf4j-log4j12")
  ))
  .settings(libraryDependencies ++= Seq(
    "ru.avicomp" % "ontapi" % "1.3.2",
    "org.apache.jena" % "jena-tdb2" % "3.9.0",
    "org.apache.jena" % "jena-fuseki" % "3.9.0",
    "org.apache.jena" % "jena-rdfconnection" % "3.9.0",
    "net.sourceforge.owlapi" % "owlapi-distribution" % "5.1.8"
      exclude("org.apache.httpcomponents", "httpclient-osgi")
      exclude("org.apache.httpcomponents", "httpcore-osgi")
      exclude("org.slf4j", "jcl-over-slf4j")
  ))
  .settings(
    scalaJSProjects := Seq(client),
    pipelineStages in Assets := Seq(scalaJSPipeline),
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    WebKeys.packagePrefix in Assets := "public/",
    managedClasspath in Runtime += (packageBin in Assets).value)
  .settings(sourceGenerators in Assets += (additionalAssets in client).taskValue)
  .dependsOn(sharedJVM)
  .enablePlugins(SbtWeb, JavaAppPackaging)

onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value
