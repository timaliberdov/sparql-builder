# SPARQL Query Builder

### Запуск RDF-хранилища:
1) docker-compose -f docker/docker-compose.yml up
<br>либо
2) скачать и установить локально apache jena fuseki

### Сборка приложения:
sbt universal:packageBin
<br>после этого в папке server/target/universal появится файл server-0.1.zip внутри которого в папке bin будет лежать бинарник и .bat файл

### Запуск приложения:
1) запуск бинарника из предыдущего пункта: ./server -f \*путь_до_файла_с_онтологией\*
<br>пока загружаются только .owl
2) запуск без сборки: sbt run -f \*путь_до_файла_с_онтологией\*
3) запуск заранее собранного бинарника из репозитория (artifacts/server-0.1.zip)

Настройки можно поменять в docker-compose.yml и в server/src/main/resources/application.conf
