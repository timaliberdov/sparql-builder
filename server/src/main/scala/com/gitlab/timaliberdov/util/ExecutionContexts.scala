package com.gitlab.timaliberdov.util

import java.util.concurrent.{ExecutorService, Executors}

import cats.effect.{Resource, Sync}

import scala.concurrent.ExecutionContext

object ExecutionContexts {

  def fixedThreadPool[F[_]](size: Int)(implicit sf: Sync[F]): Resource[F, ExecutionContext] = {
    val acquire = sf.delay(Executors.newFixedThreadPool(size))
    val release = (es: ExecutorService) => sf.delay(es.shutdown())
    Resource.make(acquire)(release).map(ExecutionContext.fromExecutor)
  }

}
