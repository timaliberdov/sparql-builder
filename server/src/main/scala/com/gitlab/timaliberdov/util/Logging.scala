package com.gitlab.timaliberdov.util

import org.slf4j.{Logger, LoggerFactory}

trait Logging {

  private val logger: Logger = LoggerFactory.getLogger(this.getClass)

  protected def info(msg: String): Unit = logger.info(msg)

  protected def debug(msg: String): Unit = logger.debug(msg)

  protected def trace(msg: String): Unit = logger.trace(msg)

  protected def warn(msg: String): Unit = logger.warn(msg)

  protected def warn(msg: String, ex: Throwable): Unit = logger.warn(msg, ex)

  protected def error(msg: String): Unit = logger.error(msg)

  protected def error(msg: String, ex: Throwable): Unit = logger.error(msg, ex)

}
