package com.gitlab.timaliberdov.config

final case class HttpConfig(host: String, port: Int)
