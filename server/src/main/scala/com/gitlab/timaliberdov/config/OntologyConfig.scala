package com.gitlab.timaliberdov.config

final case class OntologyConfig(defaultIris: List[String], supportedExtensions: Set[String])
