package com.gitlab.timaliberdov.config

import cats.effect.Sync
import cats.implicits._
import pureconfig.generic.auto._

case class ApplicationConfig(fuseki: FusekiConfig, http: HttpConfig, ontology: OntologyConfig)

object ApplicationConfig {
  def loadConfig[F[_]](implicit S: Sync[F]): F[ApplicationConfig] =
    S.delay {
      pureconfig.ConfigSource.default.at(namespace = "sparql-builder").load[ApplicationConfig]
        .leftMap(pureconfig.error.ConfigReaderException[ApplicationConfig])
    }.flatMap(S.fromEither)
}
