package com.gitlab.timaliberdov.config

final case class FusekiConfig(host: String, port: Int, dataset: String) {
  lazy val fullDatasetAddress: String = s"$host:$port/$dataset"
}
