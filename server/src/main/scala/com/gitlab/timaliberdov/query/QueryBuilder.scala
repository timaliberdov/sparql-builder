package com.gitlab.timaliberdov.query

import better.files.File
import cats.effect.concurrent.Ref
import cats.effect.{Resource, Sync}
import cats.implicits._
import com.gitlab.timaliberdov.config.{FusekiConfig, OntologyConfig}
import com.gitlab.timaliberdov.model._
import com.gitlab.timaliberdov.query.QueryBuilder.SelectResult
import com.gitlab.timaliberdov.schema.Schema
import com.gitlab.timaliberdov.util.Logging
import org.apache.jena.query.{QueryExecution, QuerySolution}
import org.apache.jena.rdf.model.RDFNode
import org.apache.jena.rdfconnection.{RDFConnection, RDFConnectionFactory}

import scala.collection.JavaConverters._

class QueryBuilder[F[_]: Sync] private (fusekiData: String, schemaRef: Ref[F, Schema[F]]) extends Logging {

  private def withConnection[T](f: RDFConnection => F[T]): F[T] =
    Resource
      .fromAutoCloseable(Sync[F].delay(RDFConnectionFactory.connect(fusekiData)))
      .use(f(_))

  private def init: F[QueryBuilder[F]] =
    withConnection { conn =>
      schemaRef.get.map(schema => conn.load(schema.modelGraph))
    }.as(this)

  private def withQuery[T](queryStr: String)(f: QueryExecution => F[T]): F[T] = withConnection { conn =>
    Resource
      .fromAutoCloseable(Sync[F].delay(conn.query(queryStr)))
      .use(f(_))
  }

  private def sparqlSelect(queryStr: String): F[List[QuerySolution]] =
    withQuery(queryStr)(_.execSelect().asScala.toList.pure[F])

  private val sparqlQueryForProperties =
    """prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      |select distinct ?p ?label
      |where {
      |  { ?s ?p ?o } .
      |  optional {
      |    ?p rdfs:label ?label
      |  }
      |} order by ?p""".stripMargin

  private val sparqlQueryForAllClasses = """prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                                           |prefix owl: <http://www.w3.org/2002/07/owl#>
                                           |
                                           |select *
                                           |where {
                                           |    { ?uri a rdfs:Class } union { ?uri a owl:Class } .
                                           |    filter ( !isBlank(?uri) ) .
                                           |    optional { ?uri rdfs:label ?label . bind(lang(?label) as ?label_lang) } .
                                           |    optional { ?uri rdfs:comment ?comment . bind(lang(?comment) as ?comment_lang) }
                                           |}
                                           |order by ?uri""".stripMargin

  def getAllClasses: F[List[Class]] = for {
    classSolutions <- sparqlSelect(sparqlQueryForAllClasses)
    classes = classSolutions.map(classFromQuerySolution)
  } yield classes

  def getAllLabelledClasses: F[List[Class]] =
    getAllClasses.map(_.filterNot(_.label.isEmpty))

  def getAllProps: F[List[Class]] = for {
    classSolutions <- sparqlSelect(sparqlQueryForAllClasses)
  } yield classSolutions.map(classFromQuerySolution)

  def getAllLabelledProps: F[List[LabelledPredicate]] = {
    for {
      _ <- Sync[F].delay(debug("QueryBuilder -> getAllLabelledProps"))
      propsSolutions <- sparqlSelect(sparqlQueryForProperties)
      _ <- Sync[F].delay(debug(s"Got ${propsSolutions.size} properties"))
    } yield propsSolutions.map(labelledPredicateFromQuerySolution)
  }

  def loadIris(newIris: List[String]): F[Unit] = (for {
    res <- schemaRef.update(schema => schema.copy(schemaIris = schema.schemaIris ++ newIris))
  } yield res).recover { case e =>
    warn("Error during schema update", e)
  }

  def generateSelect(propsToFind: List[Prop], knownProps: List[PropWithVal]): String = {
    s"""select distinct ?s ?s_label ?p ?p_label ?o ?o_label where {
       |  ?s ?p ?o ${if (knownProps.nonEmpty) { ";\n     " + knownProps.mkString(" ;\n     ") + (if (knownProps.nonEmpty) " ." else "") } else "." }
       |  ${val nonEmpty = propsToFind.nonEmpty; (if (nonEmpty) "values ?p {\n    " else "") + propsToFind.mkString("\n    ") + (if (nonEmpty) "\n  }" else "") } .
       |  optional { ?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#label> ?s_label } .
       |  optional { ?p <http://www.w3.org/1999/02/22-rdf-syntax-ns#label> ?p_label } .
       |  optional { ?o <http://www.w3.org/1999/02/22-rdf-syntax-ns#label> ?o_label }
       |}""".stripMargin
  }

  def executeSelect(request: QueryRequest): F[List[QueryResponse]] = for {
    queryString <- Sync[F].delay(generateSelect(request.propsToFind, request.knownProps))
    solutions <- sparqlSelect(queryString)
  } yield solutions.map(queryResponseFromQuerySolution)

  def executeSelect(query: String): F[List[SelectResult]] = for {
    solutions <- sparqlSelect(query)
  } yield solutions map { qs =>
    def value(node: RDFNode): String =
      if (node == null) "" else
      if (node.isLiteral) node.asLiteral().getString
      else if (node.isResource) node.asResource().getURI
      else ""
    val subj = value(qs.get("s"))
    val pred = value(qs.get("p"))
    val obj = value(qs.get("o"))
    SelectResult(subj, pred, obj)
  }

  private def labelledPredicateFromQuerySolution(qs: QuerySolution): LabelledPredicate = {
    def value(node: RDFNode): String =
      if (node.isLiteral) node.asLiteral().getString
      else if (node.isResource) node.asResource().getURI
      else ""
    def getOption(name: String): Option[String] = Option(qs.get(name)).map(value).filterNot(_.isEmpty)
    LabelledPredicate(value(qs.get("p")), getOption("label"))
  }

  private def queryResponseFromQuerySolution(qs: QuerySolution): QueryResponse = {
    def value(node: RDFNode): String =
      if (node.isLiteral) node.asLiteral().getString
      else if (node.isResource) node.asResource().getURI
      else ""
    def getOption(name: String): Option[String] = Option(qs.get(name)).map(value).filterNot(_.isEmpty)
    QueryResponse(
      subj = LabelledSubject(value(qs.get("s")), getOption("s_label")),
      pred = LabelledPredicate(value(qs.get("p")), getOption("p_label")),
      obj = LabelledObject(value(qs.get("o")), getOption("o_label"))
    )
  }

  private def classFromQuerySolution(qs: QuerySolution): Class = {
    def value(node: RDFNode): String =
      if (node.isLiteral) node.asLiteral().getString
      else if (node.isResource) node.asResource().getURI
      else ""
    def getOption(name: String): Option[String] = Option(qs.get(name)).map(value).filterNot(_.isEmpty)

    Class(
      uri = value(qs.get("uri")),
      label = getOption("label"),
      comment = getOption("comment"))
  }

}

object QueryBuilder extends Logging {
  def instance[F[_]: Sync](fusekiConfig: FusekiConfig,
                           ontologyConfig: OntologyConfig,
                           ontologyFile: Option[File]): F[QueryBuilder[F]] = {
    val defaultIris = ontologyConfig.defaultIris ++ ontologyFile
      .filterNot(_.isDirectory)
      .filter(_.extension.exists(ontologyConfig.supportedExtensions.contains))
      .toList
      .map(_.path.toAbsolutePath.toUri.toString)

    for {
      schema <- Schema.create(defaultIris)
      schemaRef <- Ref.of(schema)
      builder = new QueryBuilder[F](fusekiConfig.fullDatasetAddress, schemaRef)
      res <- builder.init
    } yield res
  }

  final case class SelectData(propsToFind: List[Prop], knownProps: List[PropWithVal])
  final case class SelectResult(subj: String, pred: String, obj: String)
}
