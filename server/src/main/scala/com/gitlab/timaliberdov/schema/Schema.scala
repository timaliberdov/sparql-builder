package com.gitlab.timaliberdov.schema

import cats.data.{NonEmptyList, Validated}
import cats.effect.{Resource, Sync}
import cats.implicits._
import com.gitlab.timaliberdov.util.Logging
import org.apache.jena.query.{QueryExecutionFactory, QueryFactory, QuerySolution}
import org.semanticweb.owlapi.formats.PrefixDocumentFormat
import org.semanticweb.owlapi.model.{IRI, OWLOntologyAlreadyExistsException}
import ru.avicomp.ontapi.jena.model.OntGraphModel
import ru.avicomp.ontapi.{OntManagers, OntologyManager, OntologyModel}

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

final case class Schema[F[_]: Sync] private (schemaIris: List[String], ontModel: OntologyModel) {

  def modelGraph: OntGraphModel = ontModel.asGraphModel()

  def sparqlSelect(queryStr: String): F[List[QuerySolution]] = {
    val query = QueryFactory.create(queryStr)
    val qExecResource = Resource.fromAutoCloseable {
      Sync[F].delay(QueryExecutionFactory.create(query, ontModel.asGraphModel()))
    }
    qExecResource.use(_.execSelect().asScala.toList.pure[F])
  }

}

object Schema extends Logging {
  import cats.syntax.validated._

  type ErrorOr[A] = Validated[NonEmptyList[String], A]

  // todo
  def validate[A](block: => A): ErrorOr[A] = Try(block) match {
    case Success(result) => result.validNel
    case Failure(f) => f.getMessage.invalidNel
  }

  def create[F[_]: Sync](schemaIris: List[String], ontologyManager: OntologyManager = OntManagers.createONT()): F[Schema[F]] = {

    def addToSchema(ontModel: OntologyModel, schemaPrefixManager: PrefixDocumentFormat)(originalOntologyModel: OntologyModel): F[Unit] = for {
      _ <- Sync[F].delay(ontModel.addAxioms(originalOntologyModel.axioms()))
      originalOntologyFormat = ontologyManager.getOntologyFormat(originalOntologyModel)
      _ <-
        if (originalOntologyFormat.isPrefixOWLDocumentFormat)
          Sync[F].delay(schemaPrefixManager.copyPrefixesFrom(originalOntologyFormat.asPrefixOWLDocumentFormat()))
        else ().pure[F]
    } yield ()

    for {
      _ <- Sync[F].delay(debug("Creating Schema"))
      ontModel <- Sync[F].delay(ontologyManager.createOntology())
      _ <- Sync[F].delay(debug("Created ontology"))
      schemaPrefixManager <- Sync[F].delay(
        ontologyManager.getOntologyFormat(ontModel).asPrefixOWLDocumentFormat())
      ontologies <- schemaIris.traverse(loadOntologyFromIri(ontologyManager)(_))
      _ <- ontologies.traverse(addToSchema(ontModel, schemaPrefixManager)(_)) // todo: handle errors
      _ <- Sync[F].delay(debug("Created schema"))
    } yield Schema(schemaIris, ontModel)
  }

  private def loadOntologyFromIri[F[_]: Sync](ontologyManager: OntologyManager)(schemaIri: String): F[OntologyModel] =
    (for {
      iri <- IRI.create(schemaIri).pure[F]
      _ <- Sync[F].delay(debug(s"Loading $iri"))
      res <- Sync[F].delay(loadOntologyFromIri(ontologyManager, iri))
      _ <- Sync[F].delay(debug(s"Loaded $iri"))
    } yield res).recoverWith { case e: OWLOntologyAlreadyExistsException =>
      Sync[F].delay(warn(s"Parsing of $schemaIri failed", e)) *>
        Sync[F].delay(ontologyManager.getOntology(IRI.create(schemaIri)))
    }

  private def loadOntologyFromIri(ontologyManager: OntologyManager, iri: IRI): OntologyModel =
    ontologyManager.loadOntologyFromOntologyDocument(iri)

}