package com.gitlab.timaliberdov

import better.files.File
import cats.effect.ExitCode
import cats.implicits._
import com.gitlab.timaliberdov.config.ApplicationConfig
import com.gitlab.timaliberdov.query.QueryBuilder
import com.gitlab.timaliberdov.service.AppService
import com.gitlab.timaliberdov.util.{ExecutionContexts, Logging}
import monix.eval.{Task, TaskApp}
import org.http4s.server.blaze.BlazeServerBuilder
import scopt.OParser

import scala.concurrent.duration._

object Server extends TaskApp with Logging {

  final case class CliConf(ontologyFile: Option[File] = None)

  override def run(args: List[String]): Task[ExitCode] =
    ExecutionContexts.fixedThreadPool[Task](size = 32).use { blockingEC =>
      for {
        _ <- Task(debug("Starting"))
        cliParserBuilder = OParser.builder[CliConf]
        cliParser = OParser.sequence(
          cliParserBuilder.opt[java.io.File]('f', "file") // -f --file
            .optional()
            .action((file, config) => config.copy(ontologyFile = File(file.getAbsolutePath).some))
            .text("Path to the ontology file to load")
        )
        cliConfig = OParser.parse(cliParser, args, CliConf())
        _ <- Task(debug(s"Loaded cli config: $cliConfig"))
        applicationConfig <- ApplicationConfig.loadConfig
        _ <- Task(debug(s"Loaded config: $applicationConfig"))
        queryBuilder <- QueryBuilder.instance[Task](
          applicationConfig.fuseki,
          applicationConfig.ontology,
          cliConfig.flatMap(_.ontologyFile))
        _ <- Task(debug(s"Created query builder: $queryBuilder"))
        appServiceRoutes <- Task.delay(new AppService[Task](queryBuilder, blockingEC).routes)
        exitCode <- BlazeServerBuilder[Task]
          .bindHttp(applicationConfig.http.port, applicationConfig.http.host)
          .withHttpApp(appServiceRoutes)
          .withIdleTimeout(5.minutes)
          .withResponseHeaderTimeout(5.minutes)
          .resource
          .use(_ => Task.never)
          .as(ExitCode.Success)
      } yield exitCode
    }
}
