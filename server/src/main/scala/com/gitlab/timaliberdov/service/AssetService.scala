package com.gitlab.timaliberdov.service

import cats.data.OptionT
import cats.effect.{ContextShift, Sync}
import cats.syntax.option._
import org.http4s.{HttpRoutes, StaticFile}

import scala.concurrent.ExecutionContext

class AssetService[F[_]: Sync: ContextShift](blockingEC: ExecutionContext) extends BaseService[F] {
  def route: HttpRoutes[F] = HttpRoutes.of {
    case request @ GET -> Root / "assets" / file =>
      (for {
        url <- OptionT.fromOption[F](Option(getClass.getClassLoader.getResource(s"public/$file")))
        response <- StaticFile.fromURL(url, blockingEC, request.some)
      } yield response) getOrElseF NotFound()
  }
}
