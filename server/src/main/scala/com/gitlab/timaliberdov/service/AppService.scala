package com.gitlab.timaliberdov.service

import cats.data.Kleisli
import cats.effect.{ContextShift, Sync}
import cats.implicits._
import com.gitlab.timaliberdov.query.QueryBuilder
import org.http4s.scalatags._
import org.http4s.server.Router
import org.http4s.syntax.kleisli._
import org.http4s.{HttpRoutes, Request, Response}
import scalatags.Text.all._
import scalatags.Text.tags.html
import scalatags.Text.tags2

import scala.concurrent.ExecutionContext

class AppService[F[_]: Sync: ContextShift](queryBuilder: QueryBuilder[F], blockingEC: ExecutionContext) extends BaseService[F] {

  def routes: Kleisli[F, Request[F], Response[F]] =
    Router[F](
      "/" -> reduceToRoute(this, new AssetService[F](blockingEC)),
      "/api" -> reduceToRoute(new QueryService[F](queryBuilder, blockingEC))
    ).orNotFound

  private def reduceToRoute(services: BaseService[F]*): HttpRoutes[F] =
    services.map(_.route).reduceLeft(_ <+> _)

  def route: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root =>
      Ok {
        html(
          head(
            tags2.title("Sparql Query Builder"),
            meta(name := "viewport", content := "width=device-width, initial-scale=1"),
            link(rel := "stylesheet", href := "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css"),
            link(rel := "stylesheet", href := "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"),
            link(rel := "stylesheet", href := "assets/main.css")),
          body(
            script(src := "https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"),
            script(src := "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"),
            script(src := "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"),
            div(id := "root"),
            raw(
              scalajs.html.scripts(
                projectName = "client",
                assets = name => s"/assets/$name",
                resourceExists = name => getClass.getResource(s"/public/$name") != null).body)
          ))
      }
  }

}
