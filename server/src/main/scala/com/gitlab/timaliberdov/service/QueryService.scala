package com.gitlab.timaliberdov.service

import cats.effect.{ContextShift, Sync}
import cats.implicits._
import com.gitlab.timaliberdov.model.QueryRequest
import com.gitlab.timaliberdov.query.QueryBuilder
import io.circe._
import org.http4s.circe._
import org.http4s.{EntityDecoder, EntityEncoder, HttpRoutes}

import scala.concurrent.ExecutionContext

class QueryService[F[_]: Sync: ContextShift](queryBuilder: QueryBuilder[F], blockingEC: ExecutionContext) extends BaseService[F] {

  private implicit def listJsonEncoder[A: Encoder]: EntityEncoder[F, List[A]] =
    org.http4s.circe.jsonEncoderOf[F, List[A]]
  private implicit def listJsonDecoder[A: Decoder]: EntityDecoder[F, List[A]] =
    org.http4s.circe.jsonOf[F, List[A]]

  private implicit val queryRequestEntityDecoder: EntityDecoder[F, QueryRequest] = jsonOf

  def route: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root / "classes" =>
      queryBuilder.getAllClasses >>= (Ok(_))
    case GET -> Root / "classes" / "labelled" =>
      queryBuilder.getAllLabelledClasses >>= (Ok(_))
    case GET -> Root / "props" =>
      queryBuilder.getAllLabelledProps >>= (Ok(_))
    case req@POST -> Root / "query" =>
      req.as[QueryRequest].map(qr => queryBuilder.generateSelect(qr.propsToFind, qr.knownProps)) >>= (Ok(_))
    case req@POST -> Root / "query" / "results" =>
      req.as[QueryRequest].map(qr => queryBuilder.executeSelect(qr)) >>= (Ok(_))
  }

}
