package com.gitlab.timaliberdov.service

import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

trait BaseService[F[_]] extends Http4sDsl[F] {
  def route: HttpRoutes[F]
}
