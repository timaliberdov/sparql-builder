addSbtPlugin("org.scala-js"       % "sbt-scalajs"               % "0.6.26")
addSbtPlugin("com.vmunier"        % "sbt-web-scalajs"           % "1.0.8-0.6")
addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject"  % "0.6.0")
addSbtPlugin("com.typesafe.sbt"   % "sbt-native-packager"       % "1.5.1")
