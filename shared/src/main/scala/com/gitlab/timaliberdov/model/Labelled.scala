package com.gitlab.timaliberdov.model

sealed abstract class Labelled(value: String, label: Option[String])

final case class LabelledSubject(subj: String, label: Option[String] = None) extends Labelled(subj, label)

final case class LabelledPredicate(pred: String, label: Option[String] = None) extends Labelled(pred, label)

final case class LabelledObject(obj: String, label: Option[String] = None) extends Labelled(obj, label)

object Labelled {
  import io.circe.{Decoder, Encoder}
  import io.circe.derivation._

  implicit val labelledSubjectEncoder: Encoder[LabelledSubject] = deriveEncoder
  implicit val labelledSubjectDecoder: Decoder[LabelledSubject] = deriveDecoder

  implicit val labelledPredicateEncoder: Encoder[LabelledPredicate] = deriveEncoder
  implicit val labelledPredicateDecoder: Decoder[LabelledPredicate] = deriveDecoder

  implicit val labelledObjectEncoder: Encoder[LabelledObject] = deriveEncoder
  implicit val labelledObjectDecoder: Decoder[LabelledObject] = deriveDecoder
}
