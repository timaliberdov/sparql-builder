package com.gitlab.timaliberdov.model

final case class Prop(iri: String) {
  override def toString: String = s"<$iri>"
}

object Prop {
  import io.circe.{Decoder, Encoder}
  import io.circe.derivation._

  implicit val propEncoder: Encoder[Prop] = deriveEncoder
  implicit val propDecoder: Decoder[Prop] = deriveDecoder
}
