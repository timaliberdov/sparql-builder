package com.gitlab.timaliberdov.model

final case class PropWithVal(prop: Prop, value: String) { // todo: value should be polymorphic
  override def toString: String = s"$prop '$value'"
}

object PropWithVal {
  import io.circe.{Decoder, Encoder}
  import io.circe.derivation._

  implicit val propWithValEncoder: Encoder[PropWithVal] = deriveEncoder
  implicit val propWithValDecoder: Decoder[PropWithVal] = deriveDecoder
}
