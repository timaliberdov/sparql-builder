package com.gitlab.timaliberdov.model

final case class Class(uri: String, label: Option[String], comment: Option[String])

object Class {
  import io.circe.{Decoder, Encoder}
  import io.circe.derivation._

  implicit val classEncoder: Encoder[Class] = deriveEncoder
  implicit val classDecoder: Decoder[Class] = deriveDecoder
}
