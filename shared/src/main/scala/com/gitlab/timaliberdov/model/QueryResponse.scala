package com.gitlab.timaliberdov.model

final case class QueryResponse(subj: LabelledSubject, pred: LabelledPredicate, obj: LabelledObject)

object QueryResponse {
  import io.circe.{Decoder, Encoder}
  import io.circe.derivation._

  implicit val queryResponseEncoder: Encoder[QueryResponse] = deriveEncoder
  implicit val queryResponseDecoder: Decoder[QueryResponse] = deriveDecoder
}
