package com.gitlab.timaliberdov.model

final case class QueryRequest(propsToFind: List[Prop], knownProps: List[PropWithVal])

object QueryRequest {
  import io.circe.{Decoder, Encoder}
  import io.circe.derivation._

  implicit val queryRequestEncoder: Encoder[QueryRequest] = deriveEncoder
  implicit val queryRequestDecoder: Decoder[QueryRequest] = deriveDecoder
}
